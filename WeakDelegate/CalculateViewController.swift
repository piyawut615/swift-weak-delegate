//
//  CalculateViewController.swift
//  WeakDelegate
//
//  Created by Piyawut Kamwiset on 11/13/2560 BE.
//  Copyright © 2560 Piyawut Kamwiset. All rights reserved.
//

import UIKit

class CalculateViewController: UIViewController {

    
    @IBOutlet weak var lblResult: UILabel!
    
    @IBOutlet weak var txtFirstInput: UITextField!
    @IBOutlet weak var txtSecondInput: UITextField!
    
    var calculatePresenter: CalculatePresenter?
    override func viewDidLoad() {
        super.viewDidLoad()
        calculatePresenter = CalculatePresenter()
        calculatePresenter?.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func clickCalculate(_ sender: Any) {
        
        let firstInput = txtFirstInput.text
        let secondInput = txtSecondInput.text
        
        calculatePresenter?.plusFunction(firstInput: firstInput!, secondInput: secondInput!)
    }
}

extension CalculateViewController: CalculatePresenterDelegate{
    func calculateResult(result: Int){
        lblResult.text = "\(result)"
    }
}
