//
//  CalculatePresneter.swift
//  WeakDelegate
//
//  Created by Piyawut Kamwiset on 11/13/2560 BE.
//  Copyright © 2560 Piyawut Kamwiset. All rights reserved.
//

import Foundation


protocol CalculatePresenterDelegate: class {
    func calculateResult(result: Int)
}


class CalculatePresenter {
    weak var delegate: CalculatePresenterDelegate?
    
    func plusFunction(firstInput: String , secondInput: String){
        let intFirstInput: Int = Int(firstInput) ?? 0
        let intSecondInput: Int = Int(secondInput) ?? 0
        let result: Int = intFirstInput + intSecondInput
        
        delegate?.calculateResult(result: result)
    }
}
